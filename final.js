const express = require("express");
const app = express();

//  dummy todolist
let todosList = [
  { id: 1, task: "task1" },
  { id: 2, task: "task2" },
  { id: 3, task: "task3" },
  { id: 4, task: "task4" },
  { id: 5, task: "task5" },
];

//  parse form data
app.use(express.urlencoded({ extended: false }));
//  parse json
app.use(express.json());

//  Home
app.get("/", (req, res) => {
  res
    .status(200)
    .send(
      '<a href="/display"> display </a> \n <a href="/add"> add </a> \n <a href="/update"> update </a> \n <a href="/delete"> delete </a>'
    );
});

//  display/get
app.get("/display", (req, res) => {
  res.status(200).send(todosList);
});

//  add/post
/*
to add a task to the list 
post > body > raw > json
{
    "Taskname": "The task you want to update "
}
*/
app.post("/add", (req, res) => {
  console.log(req);
  const newTask = {
    id: todosList.length + 1,
    task: req.body.Taskname,
  };
  todosList.push(newTask);
  res.status(200).json({ success: true, data: todosList });
});

//  update/put
/*
to update a task on the list 
put > body > raw > json
{
    "Taskname": "Task you want to edit ",
    "Newtask": "New task"
}
*/
app.put("/update", (req, res) => {
  const { Taskname } = req.body;
  const { Newtask } = req.body;

  const taskUpdate = todosList.find(
    (taskUpdate) => taskUpdate.task === Taskname
  );

  if (!taskUpdate) {
    return res
      .status(404)
      .json({ success: false, msg: `no task with name ${Taskname}` });
  }
  todosList = todosList.map((taskUpdate) => {
    if (taskUpdate.task === Taskname) {
      taskUpdate.task = Newtask;
    }
    return taskUpdate;
  });
  res.status(200).json({ success: true, data: todosList });
});

//  delete
/*
to delete a task on the list 
delete > body > raw > json
{
    "Taskname": "The task you want to delete "
}
*/
app.delete("/delete", (req, res) => {
  const taskDelete = todosList.find(
    (taskDelete) => taskDelete.task === req.body.Taskname
  );
  if (!taskDelete) {
    return res
      .status(404)
      .json({ success: false, msg: `no task with name ${req.body.Taskname}` });
  }
  todosList = todosList.filter(
    (taskDelete) => taskDelete.task !== req.body.Taskname
  );
  return res.status(200).json({ success: true, data: todosList });
});

//  listen
app.listen(3000, () => {
  console.log("Your are now listening on port 3000...");
});
